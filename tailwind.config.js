module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
   
    extend: {

      colors: {
        orgreen: '#A5D357',
        orgray: '#E5EAEE',
        orlightgray: '#F4F6F9',
        ordarkgray: '#484C4E',
        orsmallgray: '#989898',
        osmedgray: '#CED2D6',
        orpurple: '#BB98FF',
        orblue:   '#349BFE',
        orblack:  '#151515',
        orlightblue: '#1AC5BD'
  
      },
      borderRadius: {
       'large': '18px',
      },
      height: {
        '143': '143px',
        '164': '164px',
        '180': '180px',
        '244': '244px',
        '400': '400px'
       },
       minHeight: {
        '4/5' : '80%',
       },
       marginTop:{
         'titlebar' : '-100px',
        
       }

    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
