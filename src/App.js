
import './App.css';

// custom components
import Navbar from './components/Navbar'
import Dashboard from './components/Dashboard';
import Footer from './components/Footer';


function App() {
  return (
    <div className="App">
        <Navbar />
        <Dashboard />
        <Footer />
    </div>
  );
}

export default App;
