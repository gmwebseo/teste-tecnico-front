import React from 'react';

const Footer = () => {
    return (
        <footer className=" max-w-8xl w-full static bottom-0 pb-3">
                <p className="text-orsmallgray text-center">Copyright © 2021 ORpag. Todos os Direitos Reservados.</p>
        </footer>
    );
};

export default Footer;