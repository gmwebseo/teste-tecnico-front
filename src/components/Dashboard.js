import React from 'react';

/* Dashboard Cards */
import CardWelcome from './cards/CardWellcome.js';
import CardUsersValidation from './cards/CardUsersValidation.js';
import CardUsers from './cards/CardUsers.js';
import CardDeposits from './cards/CardDeposits.js';
import CardWithdraws from './cards/CardWithdraws.js';
import CardInvoices from './cards/CardInvoices.js';


/* Used on CardUsers svg prop */
import { ReactComponent as IconUser } from './assets/icons/icon_users.svg';



const Dashboard = () => {
    return (
            <div className=" max-w-8xl mx-auto  pt-10 px-4 flex justify-center">
                <div className="container grid xl:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 gap-3">

                    <CardWelcome />
                    {/* just to test how props works in React */}
                    <CardUsers svg={IconUser} title="Usuários" titlebar_color="bg-orblack"/>
                    <CardUsersValidation />
                    <CardWithdraws />
                    <CardDeposits />
                    <CardInvoices/>
                    
                </div>
            </div>
    );
};

export default Dashboard;