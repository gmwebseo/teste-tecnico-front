import React from 'react';

import { FaBell } from "react-icons/fa";

const Navbar = () => {
	return (
				
		<nav className="max-w-8xl mx-auto flex justify-center border-0 border-b border-orgray">
			<div className="container grid grid-cols-2 sm:grid-cols-2 justify-between">

				<div className="flex space-x-4">	
						<a href="/" className="flex items-center  py-5 px-2">
							<img 
									src="assets/logo_header.png"
									className=" h-7 mr-3 "
									alt="ORpag"
							/>
						</a>
				</div>

				<div className="flex items-center justify-end space-x-2 mr-4 ">     
					
					<a href="/" className="nav-btn">
						JS
					</a>

					<a href="/" className="nav-btn">
						<FaBell />
					</a>

				</div>			
			</div>		
		</nav>
		);
};
export default Navbar;