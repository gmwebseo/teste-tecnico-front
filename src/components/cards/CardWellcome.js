import React from 'react';

import { ReactComponent as IconExtrato } from '../assets/icons/icon_estrato_geral.svg';

const CardWelcome = (props) => {
    return (
        <div className="container grid grid-rows gap-0" >
                        
            <div className="box-hello">
                <p className="ml-10 text-2xl" >Olá, <strong>user@</strong></p>
            </div>

            <div className="box-extrato">
                <IconExtrato className=" ml-10 " />
                <h3 className=" text-black font-bold text-1xl "> Acesse o Extrato Geral</h3>
                <a 
                    href="/"
                    className="btnGo mr-3 -mt-32"
                >
                        {'>'}
                </a>
            </div>

        </div>
    )
};
export default CardWelcome;