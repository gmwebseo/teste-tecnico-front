import React from 'react';
import { ReactComponent as IconDepositos } from '../assets/icons/icon_depositos.svg';
const CardDeposits = (props) => {
    return (
            <div className="card">
                <div className="card-title bg-orblue">
                    <div className="ml-10 -mt-10">
                        <h3 className=" text-white text-left font-bold text-1xl ">Depósitos</h3>
                        <span className="text-black text-md">221 movimentações   </span>
                    </div>
                    <IconDepositos className="mr-10 -mt-10" />
                </div>

                <div className="card-content items-center row-span-2 -mt-5 grid grid-rows-2 grid-cols-2  text-left">
                        <div className="bg-osmedgray col-span-2 rounded-t-large h-full h-full">
                            <div className="ml-7 mt-10">
                                <span className="text-orsmallgray text-xs">Pix</span>
                                <h4 className="text-orblack  font-semibold text-1md">197</h4>
                            </div>
                        </div>
                        <div className="bg-orlightgray  rounded-bl-large h-full">
                            <div className="ml-7 mt-10">
                                <span className="text-orsmallgray text-xs">Boleto</span>
                                <h4 className="text-orblack  font-semibold text-1md">23</h4>
                            </div>
                        </div>
                        <div className="bg-orgray rounded-br-large h-full">
                            <div className="ml-7 mt-10">
                                <span className="text-orsmallgray text-xs">TED/DOC</span>
                                <h4 className="text-orblack  font-semibold text-1md">1</h4>
                            </div>
                        </div>
                </div>
            </div>
    )
};
export default CardDeposits;