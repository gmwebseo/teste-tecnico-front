import React from 'react';

import { ReactComponent as IconValidacaoUsers } from '../assets/icons/icon_validacao_users.svg';
import { ReactComponent as IconAprovado } from '../assets/icons/icon_aprovado.svg';
import { ReactComponent as IconPendente } from '../assets/icons/icon_pendente.svg';

const CardUsersValidation= (props) => {
    return (
        <div className="container grid grid-rows-6 gap-2 h-400">

            <div className="box-valid-users">
                <h3 className="ml-10  text-white font-bold text-1xl ">Validação de usuários</h3>
                <IconValidacaoUsers className="mr-10" />
            </div>

    
            <div className="box-valid-users-content">

                <div className=" bg-orlightgray rounded-l-large h-full items-center ">
                    <div className="ml-7 mt-12"> 
                        <IconPendente className="mb-3" />
                        <span className="text-orsmallgray text-xs">Pendentes</span>
                        <h4 className="text-orblack  font-semibold text-1md">1</h4>
                    </div>
                </div>

                <div className="bg-orgray rounded-r-large h-full ">
                        <div className="ml-7 mt-12">
                            <IconAprovado className="mb-3" />
                            <span className="text-orblack text-xs">Aprovados</span>
                            <h4 className="text-orblack  font-semibold text-1md">23</h4>
                        </div>
                </div>
            </div>
        </div>
    )
};
export default CardUsersValidation;