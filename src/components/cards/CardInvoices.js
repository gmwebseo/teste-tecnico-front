import React from 'react';

import { ReactComponent as IconFaturas } from '../assets/icons/icon_faturas.svg';

const CardInvoices = (props) => {
    return (
        <div className="card">

            <div className="card-title bg-orlightblue">
                    <div className="ml-10 -mt-10">
                        <h3 className=" text-white text-left font-bold text-1xl ">Faturas</h3>
                        <span className="text-black text-md">279 movimentações   </span>
                    </div>
                <IconFaturas className="mr-10  -mt-10 " />
            </div>

            <div className="card-content row-span-2 -mt-1 grid grid-rows-2 grid-cols-2 -mt-5">
            
                <div className="flex bg-orgray rounded-tl-large">
                    <div className="ml-7 mt-10 text-left"> 
                        <span className="text-orsmallgray text-xs">Pix</span>
                        <h4 className="text-orblack  font-semibold text-1md">324</h4>
                    </div>
                </div>

                <div className="flex bg-orlightgray rounded-tr-large text-left">
                    <div className="ml-7 mt-10"> 
                        <span className="text-orsmallgray text-xs">Cartão</span>
                        <h4 className="text-orblack  font-semibold text-1md">45</h4>
                    </div>
                </div>

                <div className="flex bg-orlightgray rounded-bl-large">
                    <div className="ml-7 mt-10 text-left"> 
                        <span className="text-orsmallgray text-xs">Boletos</span>
                        <h4 className="text-orblack  font-semibold text-1md">279</h4>
                    </div>
                </div>

                <div className="flex bg-gray  content-left">
                    <div className="ml-7 mt-10 text-left"> 
                        <span className="text-orsmallgray text-xs">Saques</span>
                        <h4 className="text-orblack  font-semibold text-1md">1</h4>
                    </div>
                </div>
            </div>
        </div>
    )
};
export default CardInvoices;