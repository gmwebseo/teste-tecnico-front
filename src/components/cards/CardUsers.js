import React from 'react';

const CardUsers = (props) => {
    return (
            <div className="card">

                <div className="card-title bg-orblack">
                    <h3 className="ml-10 -mt-5 text-white font-bold text-1xl ">
                        {props.title ?? "Titulo" }
                    </h3>
                    {props.svg? <props.svg  className="mr-10  -mt-5 " /> : null}
                </div>


                <div className="card-content row-span-2 -mt-1  grid grid-rows-2 grid-cols-2">

                    <div className="flex bg-orgray rounded-tl-large text-left">
                        <div className="ml-7 mt-10 "> 
                            <span className="text-orsmallgray text-xs">Total</span>
                            <h4 className="text-orblack  font-semibold text-1md">324</h4>
                        </div>
                    </div>

                    <div className="flex bg-orlightgray rounded-tr-large text-left">
                        <div className="ml-7 mt-10"> 
                            <span className="text-orsmallgray text-xs">Últimos 30 dias</span>
                            <h4 className="text-orblack  font-semibold text-1md">45</h4>
                        </div>
                    </div>

                    <div className="flex bg-orlightgray rounded-bl-large text-left">
                        <div className="ml-7 mt-10 "> 
                            <span className="text-orsmallgray text-xs">Últimos 7 dias</span>
                            <h4 className="text-orblack font-semibold text-1md">279</h4>
                        </div>
                    </div>

                    <div className="flex bg-gray  content-left text-left">
                        <div className="ml-7 mt-10 "> 
                            <span className="text-orsmallgray text-xs">Último dia</span>
                            <h4 className="text-orblack  font-semibold text-1md">1</h4>
                        </div>
                    </div>

                </div>
            </div>
    );
};

export default CardUsers;