import React from 'react';


import { ReactComponent as IconSaques } from '../assets/icons/icon_saque.svg';
import { ReactComponent as IconPIX } from '../assets/icons/icon_pix.svg';
import { ReactComponent as IconTED } from '../assets/icons/icon_ted.svg';

const CardWithdraws = (props) => {
    return (
        <div className="card">
            <div className="card-title bg-orpurple">
                <div className="ml-10 -mt-10">
                    <h3 className=" text-white text-left font-bold text-1xl ">Saques</h3>
                    <span className="text-black text-md">324 movimentações   </span>
                </div>
                <IconSaques className="mr-10  -mt-10" />
            </div>


            <div className="card-content row-span-2 -mt-5 grid grid-rows-1 grid-cols-2">
            
                <div className=" bg-orlightgray rounded-l-large h-full text-left ">
                    <div className="ml-7 mt-12 pt-4">
                        <IconPIX className="mb-1" />
                        <span className="text-orsmallgray text-xs mb-1">Pix</span>
                        <h4 className="text-orblack font-semibold text-1md">197</h4>
                    </div>
                </div>
                <div className="bg-orgray rounded-r-large h-full text-left">
                    <div className="ml-7 mt-12 pt-4 "> 
                        <IconTED className="mb-1 -ml-3" />
                        <span className="text-orsmallgray text-xs mb-1">TED</span>
                        <h4 className="text-orblack font-semibold text-1md">23</h4>
                    </div>
                </div>
            
            </div>
    </div>
    )
};
export default CardWithdraws;